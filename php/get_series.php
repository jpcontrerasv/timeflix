<?php
    include("curl_exec.php");
   
    try { 
        $json_parametros = json_decode($_POST["json"]);
         
        $pag_aleatoria = $json_parametros->pag_aleatoria;
        $duracion = $json_parametros->duracion;
  
        $query = 'https://apis.justwatch.com/content/titles/en_NL/popular?body={"age_certifications":null,"content_types":null,"genres":null,"languages":null,"max_price":null,"min_price":null,"monetization_types":["flatrate","rent","buy"],"page":'.$pag_aleatoria.',"page_size":10,"presentation_types":null,"providers":["nfx"],"release_year_from":null,"release_year_until":null,"scoring_filter_types":null,"timeline_type":null}';
        $curl_exec = new curl_exec();
        $response = $curl_exec->exec($query); 
        //quita los campos innecesarios del objeto json 
        $response = json_decode($response, false);

        //unset($response->page_size);
        unset($response->page);
        unset($response->total_pages);
        unset($response->total_results);

        $arrlength = count($response->items);
        for ($x = 0; $x < $arrlength; $x++) {        
            unset($response->items[$x]->full_paths);
            unset($response->items[$x]->short_description); 

            //si el elemento es una pelicula verifica el tiempo estimado y quita el elemento si no cumple con dicha regla
            if(isset($response->items[$x]->runtime))
            { 
               if  ($response->items[$x]->runtime > $duracion){
                    unset($response->items[$x]);
                }
            }            
        }
        $values = array_values($response->items);
        $response->items = $values;

        echo json_encode($response);

    } catch (Exception $e) {
        echo $e; 
    }

?>