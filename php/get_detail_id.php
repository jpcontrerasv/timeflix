<?php
    include("curl_exec.php");
    include("cache.php");

    try { 
        $json_parametros = json_decode($_POST["json"]); 

        $all_details = array();
             
        $index = 0;
        foreach( $json_parametros->peliculas as $key) {
            $cache = new FileCache();

            // A somewhat unique key
            $key_cache =  $key->id;

            // check if the data is not in the cache already
            if (!$data = $cache->fetch( $key_cache)) {                  
                $id = $key->id;
                $tipo =  $key->tipo;

                $query = "https://apis.justwatch.com/content/titles/" . $tipo . "/" . $id . "/locale/en_NL";
                $curl_exec = new curl_exec();
                $response = $curl_exec->exec($query);
    
                $json_data = json_decode($response);

                //quita los campos innecesarios 
                unset($json_data->monetization_type);
                unset($json_data->original_release_year);
                unset($json_data->full_paths);
                unset($json_data->short_description);
                unset($json_data->credits);
                unset($json_data->genre_ids);
                unset($json_data->max_season_number);
                unset($json_data->original_title);
                unset($json_data->scoring);
                unset($json_data->tmdb_popularity);
                unset($json_data->original_language);
                unset($json_data->poster);
                unset($json_data->title);
                unset($json_data->clips);
                unset($json_data->backdrops);
                unset($json_data->external_ids);
                unset($json_data->seasons);
                unset($json_data->full_path);
                unset($json_data->age_certification); 

                if ( $json_data->object_type == "show")
                {
                    //obtener los detalles de la pelicula/serie (liga y tiempo de duracion)
                    $query = "https://apis.justwatch.com/content/titles/show_season/" . $id . "/locale/en_NL";
                    $curl_exec = new curl_exec(); 
                    $detail_response = $curl_exec->exec($query);
                    // $data = $details->episodes[0];
                    // $duration = $data->runtime; 
                    $result_details= json_decode( $detail_response);  

                    if  (isset($result_details->episodes[0]->runtime))
                    {
                        $json_data->runtime = json_encode($result_details->episodes[0]->runtime); 
                    }else   
                    {
                        $json_data->runtime = "nd";                     
                    }
                    
                } 

                $data = $json_data->offers;
                $clave = array_search('8', array_column($data, 'provider_id'));
                $data = $json_data->offers[$clave];
                $enlace = $data->urls->standard_web;            
                $json_data->url_code = $enlace;

                unset($json_data->offers);

                $data = $json_data;
                // Storing the data in the cache for 10 minutes
                $cache->store($id, $data, 1200); 
            }
            $json_data = $data;

            $all_details[$index] = array(
                'data'    => json_encode( $json_data) 
            );

            $index++; 
        }
        
        echo json_encode($all_details);

        //echo json_encode($all_details);

    } catch (Exception $e) {
        echo $e; 
    }
?>