<?php
    try { 
        $query = 'https://apis.justwatch.com/content/titles/en_NL/popular?body={"age_certifications":null,"content_types":null,"genres":null,"languages":null,"max_price":null,"min_price":null,"monetization_types":["flatrate","rent","buy"],"page":1,"page_size":10,"presentation_types":null,"providers":["nfx"],"release_year_from":null,"release_year_until":null,"scoring_filter_types":null,"timeline_type":null}';
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL =>  $query ,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode($response, true);  
        echo $response['total_pages'];

    } catch (Exception $e) {
        echo $e; 
    } 
?>
