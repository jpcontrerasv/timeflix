      <footer class="position-fixed text-right w-100">
          <p>Timeflix no está en ningún caso asociado con Netflix. <a href="#" data-toggle="modal" data-target="#exampleModal">Créditos</a></p>
      </footer>
      
      
      <!-- Modal -->

      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  
          <div class="modal-dialog bg-dark" role="document">
    
              <div class="modal-content">
      
      
                  <div class="modal-body">
                      
                      <img src="img/logo.png" alt="" class="d-block mx-auto">
                      <p class="mb-5 ">Lorem ipsum dolor sit amet</p>
                      
                    <button type="button" class="btn btn-flix d-block mx-auto " data-dismiss="modal">Cerrar</button>
                      
                  </div>
    
              </div>
  
          </div>

      </div>
      
    <script src="js/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
      
      <!--<script type="text/javascript">
    $(window).on('load',function(){
        $('#exampleModal').modal('show');
    });
</script>-->
    
    <script src="js/bootstrap.js"></script>
    <script>
    // Creación de la petición HTTP

   $(document).ready(function(){ 




    var divs;
var pagina = 1;
$('#alphBnt').on('click', function () {
    var alphabeticallyOrderedDivs = $divs.sort(function (a, b) {
        return $(a).find("h1").text() > $(b).find("h1").text();
    });
    $("#contenido").html(alphabeticallyOrderedDivs);
});

$('#numBnt').on('click', function () {
    var numericallyOrderedDivs = $divs.sort(function (a, b) {
        return $(a).find("h2").text() - $(b).find("h2").text();
    });
    $("#contenido").html(numericallyOrderedDivs);
});


          $("#findByTimeBtn").on('click',function(){
               /* alert($("#minutes").val());*/
               $("#tiempo_buscado").html($("#minutes").val());
               $("#loa").show();
               $("#main_search").css('filter','blur(6px)');
                 findByTime($("#minutes").val());
                 
                console.log("se ejecuto peticion");

            });
     

        $("#mostrar_mas").on('click',function(){
            
            $("#tiempo_buscado").html($("#minutes").val());
               $("#loa").show();
               $("#main_search").css('filter','blur(6px)');
               mostrarMas($("#minutes").val(),$("#pagina").val());
               
        });
        
    });




       async function findByTime(time){
        var req = new XMLHttpRequest();
        cantidad_peliculas = 0;
       
        try {
            let response =  await req.open("GET", 'https://cors-anywhere.herokuapp.com/https://apis.justwatch.com/content/titles/en_NL/popular?body={"age_certifications":null,"content_types":["movie"],"genres":null,"languages":null,"max_price":null,"min_price":null,"monetization_types":["flatrate","rent","buy"],"page":1,"page_size":30,"presentation_types":null,"providers":["nfx"],"release_year_from":null,"release_year_until":null,"scoring_filter_types":null,"timeline_type":null}', false);
           // let response =  await req.open("GET", "https://api.themoviedb.org/3/discover/movie?with_runtime.lte="+time+"&api_key=158b8a4d5abf4cf97ccce105eb2a0c9a", false);
            // Envío de la petición

            req.send(null);
            // Almacenar los resultados y convertirlos a formato json
            var results = JSON.parse(req.responseText);

            //console.log(results.results[1].id);
            for(var i = 0; i < 10; i++){
                
                
                var id = results.items[i].id;
                //console.log(id);
               // let detail = await this.getDetailID(id);
                var nb_slug = results.items[i].full_path.split("/");
                var slug = nb_slug[3];
                console.log(slug);
                var ruta_imagen = results.items[i].poster.split("/");
                var imagen_id = ruta_imagen[1]+"/"+ruta_imagen[2]+"/";
                var imagen = "/"+imagen_id+"s166/"+slug;
                var titulo = results.items[i].title;
                var duracion = results.items[i].runtime;
                //alert(duracion);
                if((duracion !== null) && (duracion <= time)){
                    if(imagen !== null){
                        cantidad_peliculas++;
                        console.log("hola");
                        await this.addMovie(id,i,imagen,titulo,duracion);
                    }else{
                       console.log("no posee imagen: "+i);
                    }
                
               }else{
                   console.log("no posee duracion: "+i);
               }
               
                //console.log(user);
                if(i === 9){
                     $divs = $("div.item");
                    $("#cont_main").css('display','none');
                    
                    $("#main_resultados").show();
                    $("#loa").hide();
                    $("#main_search").css('filter','none');
                    $("#cant_resultados").html(cantidad_peliculas + " resultados.");
                    //$("#lbl_pagina").html("Página #"+(parseInt($("#pagina").val())+1));
                    $("#pagina").val(parseInt($("#pagina").val())+1);
                }
            }
        } catch(err) {
            // catches errors both in fetch and response.json
            alert(err);
        }
        
        


        }

        async function mostrarMas(time,pagina){
        var req = new XMLHttpRequest();
        cantidad_peliculas = 0;
        try {
            let response =  await req.open("GET", 'https://cors-anywhere.herokuapp.com/https://apis.justwatch.com/content/titles/en_NL/popular?body={"age_certifications":null,"content_types":["movie"],"genres":null,"languages":null,"max_price":null,"min_price":null,"monetization_types":["flatrate","rent","buy"],"page":'+pagina+',"page_size":10,"presentation_types":null,"providers":["nfx"],"release_year_from":null,"release_year_until":null,"scoring_filter_types":null,"timeline_type":null}', false);
           // let response =  await req.open("GET", "https://cors-escape.herokuapp.com/https://api.themoviedb.org/3/discover/movie?with_runtime.lte="+time+"&api_key=158b8a4d5abf4cf97ccce105eb2a0c9a", false);
            // Envío de la petición
console.dir(response);
            req.send(null);

            // Almacenar los resultados y convertirlos a formato json
            var results = JSON.parse(req.responseText);

            console.dir(results);
            for(var i = 0; i < 10; i++){
                
                
                var id = results.items[i].id;
                //console.log(id);
               // let detail = await this.getDetailID(id);
                var nb_slug = results.items[i].full_path.split("/");
                var slug = nb_slug[3];
                console.log(slug);
                var ruta_imagen = results.items[i].poster.split("/");
                var imagen_id = ruta_imagen[1]+"/"+ruta_imagen[2]+"/";
                var imagen = "/"+imagen_id+"s166/"+slug;
                var titulo = results.items[i].title;
                var duracion = results.items[i].runtime;
                //alert(duracion);
                if((duracion !== null) && (duracion <= time)){
                    if(imagen !== null){
                        cantidad_peliculas++;
                        
                        await this.addMovie(id,i,imagen,titulo,duracion);
                    }else{
                       console.log("no posee imagen: "+i);
                    }
                
               }else{
                   console.log("no posee duracion: "+i);
               }
               
                //console.log(user);
                if(i === 9){
                     $divs = $("div.item");
                    $("#cont_main").css('display','none');
                    
                    $("#main_resultados").show();
                    $("#loa").hide();
                    $("#main_search").css('filter','none');
                    $("#cant_resultados").html(cantidad_peliculas + " resultados.");
                   // $("#lbl_pagina").html("Página #"+parseInt($("#pagina").val())+1);
                    $("#pagina").val(parseInt($("#pagina").val())+1);
                }
            }
        } catch(err) {
            // catches errors both in fetch and response.json
            alert(err);
        }
        
        


        }




        async function getDetailID(id){
            //console.log("aqui");  
            var id = id;
            var s = new XMLHttpRequest();
            //await s.open("GET", "https://api.themoviedb.org/3/movie/"+id+"?&api_key=158b8a4d5abf4cf97ccce105eb2a0c9a", false);
            await s.open("GET", "https://cors-anywhere.herokuapp.com/https://apis.justwatch.com/content/titles/movie/"+id+"/locale/en_NL", false);
            s.send(null);
            
            var info_pelicula = JSON.parse(s.responseText);
            //console.log(info_pelicula);
            var enlace = info_pelicula;
           // console.log("el enlace es: " + enlace);
            return enlace;
        }

      /*  async function getDetailIDTvShow(id){
            //console.log("aqui");  
            var id = id;
            var s = new XMLHttpRequest();
            await s.open("GET", "https://api.themoviedb.org/3/tv/"+id+"?&api_key=158b8a4d5abf4cf97ccce105eb2a0c9a", false);
            s.send(null);
            console.log(s.responseText);
            var info_pelicula = JSON.parse(s.responseText);
            var duracion = info_pelicula.runtime;
            console.log("la duracion de esta pelicula es de: " + duracion + " minutos");
            return duracion;
        }
*/
        async function addMovie(id,posicion,imagen,titulo,duracion){
            
            let enl_nf = await this.getDetailID(id);
                        console.dir(enl_nf.offers);
                        var index =   enl_nf.offers.map(function(d) { return d.provider_id; }).indexOf(8);
                        console.log("EL indice es: "+index);
                       let enlace = enl_nf.offers[index].urls.standard_web;
            var urlcod = enlace;
            $('.contenido').append(`<div class="item">
<h1 hidden>${titulo}</h1>
<h2 hidden>${duracion}</h2>
<div class="img position-relative">

    <span class="position-absolute mins" >${duracion} min</span>
    <img src="https://images.justwatch.com${imagen}" alt="" width="166" height:"236">

<div class="position-absolute info">

    <h5>${titulo}</h5>

    <a href="${urlcod}" target="_blank" class="btn btn-flix" >Ver en Netflix</a> 

</div>

</div>

</div>`);
        }
    </script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133917326-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-133917326-1');
</script>


  </body>
</html>