<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog bg-dark" role="document">

        <div class="modal-content">


            <div class="modal-body">

                <img src="img/logo.svg" alt="" class="d-block mx-auto logo">

                <p class="mb-2 text-center">Timeflix quiere mejorar la experiencia de ver Netflix, ayudándote a descubrir películas y series basándonos únicamente en un factor:</p>
                <h4 class="text-center">El tiempo que tienes disponible</h4>
                <p class="text-center mb-3">Las opciones son muy aleatorias, para que siempre encuentres algo nuevo que mirar.</p>
                <br>
                <p>Idea Original:</p>
                <h5 class="mb-3">Pape González</h5>

                <p>Desarrollada por:</p>

                <h5 class="mb-2">Juan Pablo Contreras <a href="https://jpcontreras.com" target="_blank">@jpcontrerasv</a></h5>
                <h5 class="mb-2">Ricardo Altmann <a href="https://twitter.com/aelete" target="_blank">@aelete</a></h5>

                <h5 class="mb-4"><a href="https://www.linkedin.com/in/leroy-carrasquero-7ab659106/" target="_blank">Leroy Carrasquero</a></h5>

                <p class="mb-2">GIF de carga por Giphy</p>


                <p class="mb-2">Ningún copyright fue herido en la creación de este sitio. Timeflix no está DE <strong>NINGUNA MANERA</strong> relacionado con NETFLIX, pero somos muy fans del servicio.</p>

                <p class="mb-2">Versión 0.9 llamada "Eterno Beta".</p>

                <button type="button" class="btn btn-flix d-block mx-auto " data-dismiss="modal">Cerrar</button>

            </div>

        </div>

    </div>

</div>

<script src="js/jquery-1.10.2.min.js" crossorigin="anonymous"></script>

<!--
<script type="text/javascript">
$(window).on('load',function(){
$('#exampleModal').modal('show');
});
</script>
--> 

<script src="js/script.js"></script>
<script src="js/querys.js"></script>
<script src="js/bootstrap.js"></script> 

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133917326-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-133917326-1');
</script>


</body>

</html>