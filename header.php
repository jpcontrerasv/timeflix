<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">       
        <meta name="format-detection" content="telephone=no">
        <meta name="format-detection" content="date=no">
        <meta name="format-detection" content="address=no">
        
        <!--favicon-->
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#c31019">
        <meta name="msapplication-TileColor" content="#141414">
        <meta name="theme-color" content="#141414">

        <!--Social meta tags-->

        <!--Twitter-->
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@timeflixapp" />
        <meta name="twitter:creator" content="@timeflixapp" />

        <!--Facebook-->
        <meta property="og:title" content="Descubre películas y series en Netflix | Timeflix" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="http://timeflix.net/" />
        <meta property="og:image" content="http://timeflix.net/logo-sm.jpg" />
        <meta property="og:description" content="Timeflix te sugiere qué ver en Netflix, basado en el tiempo que tienes disponible. Descubre nuevas series y películas cada vez." />

        <!--Google-->
        <meta itemprop="name" content="Descubre películas y series en Netflix | Timeflix">
        <meta itemprop="description" content="Timeflix te sugiere qué ver en Netflix, basado en el tiempo que tienes disponible. Descubre nuevas series y películas cada vez.">
        <meta itemprop="image" content="http://timeflix.net/logo-sm.jpg">


        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.css" crossorigin="anonymous">
        
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        
        <link rel="stylesheet" href="style.css" crossorigin="anonymous">
        
         
        <title>Descubre películas y series en Netflix | Timeflix</title>
    </head>
    <body>

        <header class="d-flex align-items-center justify-content-between" >
            <a href="index.php"><img src="img/logo.svg" alt="" class="logo"></a>

            <p class="mr-3 d-flex align-items-center">
                <a href="https://www.facebook.com/timeflixapp/" target="_blank"><i class="fab fa-facebook-square fa-2x"></i></a>&nbsp;&nbsp;
                <a href="https://twitter.com/timeflixapp" target="_blank"><i class="fab fa-twitter-square fa-2x"></i></a>&nbsp;&nbsp;
                <a href="https://instagram.com/timeflixapp" target="_blank"><i class="fab fa-instagram fa-2x"></i></a>&nbsp;&nbsp;
                <a href="#" data-toggle="modal" data-target="#exampleModal">Créditos</a></p>

        </header>
 
<?php $arrX = array("PhvQ0pSu9MgLe", "VxbP9tLeKzazm","u7uiWWbRFC2TC", "VHW0X0GEQQjiU", "CYUDHVmioGETu", "55bM8mirLn2zC", "3CWtVCH72Fu7u", "HVr4gFHYIqeti", "xT9IghIG6yXWd64Yms", "3o7btMTC2FaJQpE9fG"); $randIndex = array_rand($arrX); ?>        
        <div id="loa" style="display:none;" class="text-center">
            <div class="wrap" style="background-image: url(https://media.giphy.com/media/<?php echo $arrX[$randIndex]; ?>/giphy.gif);">
                <p>Mira este entretenido gif mientras buscamos resultados</p>
                <!--<div class="cssload-loader">Buscando</div>-->
            </div>
        </div>
        


