    // Creación de la petición HTTP

    $(document).ready(function(){  
        // $("#loa").show();
        var divs;
        var pagina = 1;
        var n_aleatorio;
        var usados = new Array();
        //alert(usados.length);
        var cantidad_paginas;
        $('#alphBnt').on('click', function () {
            var alphabeticallyOrderedDivs = $divs.sort(function (a, b) {
                return $(a).find("h1").text() > $(b).find("h1").text();
            });
            $("#contenido").html(alphabeticallyOrderedDivs);
        });

        $('#numBnt').on('click', function () {
            var numericallyOrderedDivs = $divs.sort(function (a, b) {
                return $(a).find("h2").text() - $(b).find("h2").text();
            });
            $("#contenido").html(numericallyOrderedDivs);
        });


        $("#findByTimeBtn").on('click',function(){
            $("#loa").show();
            setTimeout(function(){
                console.log("entro find by time");
                $("#tiempo_buscado").html($("#minutes").val());
                console.log("ejecuto loa");
                $("#main_search").css('filter','blur(6px)');
                findByTime($("#minutes").val(),usados);
            },2000);

        });
        $("#mostrar_mas").on('click',function(){

            $("#tiempo_buscado").html($("#minutes").val());
            $("#loa").show();
            $("#main_search").css('filter','blur(6px)');
            mostrarMas($("#minutes").val(),n_aleatorio,usados);

        });
    });    


    async function findByTime(time,usados){ //Funcion para encontrar series por tiempo
        var req = new XMLHttpRequest();
        cantidad_peliculas = 0;
        try {  
            let response =  await req.open("GET", 'https://cors-anywhere.herokuapp.com/https://apis.justwatch.com/content/titles/en_NL/popular?body={"age_certifications":null,"content_types":null,"genres":null,"languages":null,"max_price":null,"min_price":null,"monetization_types":["flatrate","rent","buy"],"page":1,"page_size":10,"presentation_types":null,"providers":["nfx"],"release_year_from":null,"release_year_until":null,"scoring_filter_types":null,"timeline_type":null}', false);
            //let response =  await req.open("GET", "https://api.themoviedb.org/3/discover/movie?with_runtime.lte="+time+"&api_key=158b8a4d5abf4cf97ccce105eb2a0c9a", false);
            // Envío de la petición

            req.send(null);
            //obtener el resultado de la busqueda en formato crudo y quitar los slashes  
            var resultados = jQuery.parseJSON(req.responseText);  
              
            cantidad_paginas = resultados.total_pages;
            //console.log("cantidad de paginas: "+cantidad_paginas);
            //obtener el valor de paginas para consulta, luego consultamos nuevamente para hacer búsquedas aleatorias.
            n_aleatorio = await numeroAleatorio(1, cantidad_paginas,usados);
            await usados.push(n_aleatorio);

            let responses =  await req.open("GET", 'https://cors-anywhere.herokuapp.com/https://apis.justwatch.com/content/titles/en_NL/popular?body={"age_certifications":null,"content_types":null,"genres":null,"languages":null,"max_price":null,"min_price":null,"monetization_types":["flatrate","rent","buy"],"page":'+n_aleatorio+',"page_size":10,"presentation_types":null,"providers":["nfx"],"release_year_from":null,"release_year_until":null,"scoring_filter_types":null,"timeline_type":null}', false);
            //let responses =  await req.open("GET", "https://api.themoviedb.org/3/discover/movie?with_runtime.lte="+time+"&api_key=158b8a4d5abf4cf97ccce105eb2a0c9a", false);

            // Envío de la petición 
            req.send(null);
            /* almacenar los resultados y convertirlos a formato json */ 
            var results = jQuery.parseJSON(req.responseText);   
            
            //console.log(results.results[1].id);
            for(var i = 0; i < 10; i++){
 
                var id = results.items[i].id;
                var tipo = results.items[i].object_type;
                //console.log("tipo: "+tipo);
                // let detail = await this.getDetailID(id);
                var nb_slug = results.items[i].full_path.split("/");
                var slug = nb_slug[3];
                //console.log(slug);
                var imagen;
                var imagen_id; 
                var ruta_imagen;
                if(results.items[i].poster !== undefined){

                    ruta_imagen = results.items[i].poster.split("/");
                    imagen_id = ruta_imagen[1]+"/"+ruta_imagen[2]+"/";
                    imagen = "/"+imagen_id+"s166/"+slug;
                }else{
                    imagen = null;
                }
                var titulo = results.items[i].title;
                var duracion = results.items[i].runtime;
                //alert(duracion);
                if((duracion !== null) && (duracion <= time)){
                    if(imagen !== null){
                        cantidad_peliculas++;
                        //console.log("hola");
                        await this.addMovie(id,i,imagen,titulo,duracion,tipo);
                    }else{
                        console.log("no posee imagen: "+i);
                    }

                }else{
                    if(tipo === "show"){
                        if(imagen !== null){
                            cantidad_peliculas++;
                            //console.log("hola");
                            await this.addMovie(id,i,imagen,titulo,"",tipo);
                        }
                        console.log("no posee duracion: serie");
                    }
                }

                //console.log(user);
                if(i === 9){
                    $divs = $("div.item");
                    $("#cont_main").css('display','none');
                    $("#main_resultados").show();
                    $("#loa").hide();
                    $("#main_search").css('filter','none');
                    $("#cant_resultados").html(cantidad_peliculas + " resultados en esta búsqueda.");
                    //$("#lbl_pagina").html("Página #"+(parseInt($("#pagina").val())+1));
                    $("#pagina").val(parseInt($("#pagina").val())+1);
                }
            }
        } catch(err) {
            // catches errors both in fetch and response.json
            alert(err);
            
            $("#main_search").css('filter','blur(0px)');
            $("#loa").hide();
        } 
    }

    async function mostrarMas(time,pagina,usados){
        var req = new XMLHttpRequest();
        cantidad_peliculas = 0;
        //alert(usados);
        //algoritmo para validar nueva pagina que no este repetida en el array
        n_aleatorio = await numeroAleatorio(1, cantidad_paginas,usados);
        //console.log("mostrar mas: lenght usados => "+usados.length);
        await usados.push(n_aleatorio);
        try {
            let response =  await req.open("GET", 'https://cors-anywhere.herokuapp.com/https://apis.justwatch.com/content/titles/en_NL/popular?body={"age_certifications":null,"content_types":null,"genres":null,"languages":null,"max_price":null,"min_price":null,"monetization_types":["flatrate","rent","buy"],"page":'+n_aleatorio+',"page_size":10,"presentation_types":null,"providers":["nfx"],"release_year_from":null,"release_year_until":null,"scoring_filter_types":null,"timeline_type":null}', false);
            // let response =  await req.open("GET", "https://cors-escape.herokuapp.com/https://api.themoviedb.org/3/discover/movie?with_runtime.lte="+time+"&api_key=158b8a4d5abf4cf97ccce105eb2a0c9a", false);
            // Envío de la petición
            //console.dir(response);
            req.send(null);  
            // Almacenar los resultados y convertirlos a formato json 
            var results = jQuery.parseJSON(req.responseText);
          
            //console.dir(results);
            for(var i = 0; i < 10; i++){
 
                var id = results.items[i].id;
                var tipo = results.items[i].object_type;
                //console.log("tipo: "+tipo);
                //console.log(id);
                // let detail = await this.getDetailID(id);
                var nb_slug = results.items[i].full_path.split("/");
                var slug = nb_slug[3];
                //console.log(slug);
                if(results.items[i].poster !== undefined){
                    var ruta_imagen = results.items[i].poster.split("/");
                    //console.log("poster: "+results.items[i].poster);
                    var imagen_id = ruta_imagen[1]+"/"+ruta_imagen[2]+"/";
                    var imagen = "/"+imagen_id+"s166/"+slug;
                }else{
                    imagen = null;
                }

                var titulo = results.items[i].title;
                var duracion = results.items[i].runtime;
                //alert(duracion);
                if((duracion !== null) && (duracion <= time)){
                    if(imagen !== null){
                        cantidad_peliculas++;

                        await this.addMovie(id,i,imagen,titulo,duracion,tipo);
                    }else{
                        console.log("no posee imagen: "+i);
                    }

                }else{
                    //console.log("no posee duracion: "+i);
                    if(tipo === "show"){
                        if(imagen !== null){
                            cantidad_peliculas++;
                            //console.log("hola");
                            await this.addMovie(id,i,imagen,titulo,"",tipo);
                        }
                        console.log("no posee duracion: serie");
                    }
                }

                //console.log(user);
                if(i === 9){
                    $divs = $("div.item");
                    $("#cont_main").css('display','none');
                    $("#main_resultados").show();
                    $("#loa").hide();
                    $("#main_search").css('filter','none');
                    $("#cant_resultados").html(cantidad_peliculas + " resultados.");
                    // $("#lbl_pagina").html("Página #"+parseInt($("#pagina").val())+1);
                    $("#pagina").val(parseInt($("#pagina").val())+1);
                }
            }
        } catch(err) {
            // catches errors both in fetch and response.json
            alert(err);
            console.trace();
        } 
    }

     /*  async function getDetailIDTvShow(id){
            //console.log("aqui");  
            var id = id;
            var s = new XMLHttpRequest();
            await s.open("GET", "https://api.themoviedb.org/3/tv/"+id+"?&api_key=158b8a4d5abf4cf97ccce105eb2a0c9a", false);
            s.send(null);
            console.log(s.responseText);
            var info_pelicula = JSON.parse(s.responseText);
            var duracion = info_pelicula.runtime;
            console.log("la duracion de esta pelicula es de: " + duracion + " minutos");
            return duracion;
        }
*/
    async function addMovie(id,posicion,imagen,titulo,duracion,tipo){
        //console.log(tipo);
        let enl_nf = await this.getDetailID(id,tipo);
        //console.dir(enl_nf.seasons);
        if(tipo === "show"){
            let tiempo_serie = await this.getDetailIDSerie(enl_nf.seasons[0].id);
            duracion = tiempo_serie;
        }
        var index =   enl_nf.offers.map(function(d) { return d.provider_id; }).indexOf(8);

        //console.log("EL indice es: "+index);
        let enlace = enl_nf.offers[index].urls.standard_web;

        var urlcod = enlace; //"enlace"; 
        $('.contenido').append(`<div class="item">
            <h1 hidden>${titulo}</h1>
            <h2 hidden>${duracion}</h2>
            <div class="img position-relative">

            <span class="position-absolute mins" >${duracion} min</span>
            <img src="https://images.justwatch.com${imagen}" alt="" width="166" height:"236">

            <div class="position-absolute info">

            <h5>${titulo}</h5>

            <a href="${urlcod}" target="_blank" class="btn btn-flix" >Ver en Netflix</a> 

                </div>

                </div>

                </div>`);
        //test para la llamada a la api a travez de php 
        mostrarDetalles(id, tipo);
       
    }

    function mostrarDetalles(id, tipo)
    {
        $.post("php/movieDetails.php", { id: id, tipo: tipo} )
           .done(function (data) {
               console.log(data)
           })
           .fail(function (data) {
               console.log('Error de comunicaciones');
           }); 
    }
    
    async function getDetailID(id,tipo){
        //console.log("aqui");  
         
        var id = id;
        var s = new XMLHttpRequest();
        //await s.open("GET", "https://api.themoviedb.org/3/movie/"+id+"?&api_key=158b8a4d5abf4cf97ccce105eb2a0c9a", false);
        var apicall = "https://cors-anywhere.herokuapp.com/https://apis.justwatch.com/content/titles/"+tipo+"/"+id+"/locale/en_NL";
        await s.open("GET", apicall, false);
        s.send(null);                
      
        var rawInfoPelicula = dropSlashes(s.responseText);  

        var info_pelicula = jQuery.parseJSON(rawInfoPelicula);
        
        //console.log(info_pelicula);
        var enlace = info_pelicula;
        //console.log("el enlace es: " + enlace);
        return enlace;
    }

    async function getDetailIDSerie(id){
        //console.log("aqui");  
        var id = id;
        var s = new XMLHttpRequest();
        //await s.open("GET", "https://api.themoviedb.org/3/movie/"+id+"?&api_key=158b8a4d5abf4cf97ccce105eb2a0c9a", false);
        await s.open("GET", "https://cors-anywhere.herokuapp.com/https://apis.justwatch.com/content/titles/show_season/"+id+"/locale/en_NL", false);
        s.send(null);
 
        var rawInfoPelicula = dropSlashes(s.responseText); 
        var info_pelicula = jQuery.parseJSON(rawInfoPelicula);

        //console.log(info_pelicula);
        var info_tempo = info_pelicula;
        //console.dir(info_tempo);
        return info_tempo.episodes[0].runtime;
    }

    // si el tipo es una entonces devuelve solo el enlace de netflix, si ademas el tipo es    
    function getDetailsMovieSerie(tipo, rawJson)
    {
        $.ajax({ url: '../php/',
         data: {action: 'test'},
         type: 'post',
         success: function(output) {
                      alert(output);
                  }
        });
    }

    //funcion para definir el número aleatorio
    async function numeroAleatorio(min, max,usados) {
        var num = Math.round(Math.random() * (max - min) + min);
        var existe = false;
        //console.log("imprimienod usados:");
        //console.log("longitud: "+this.usados.length);
        if(usados.length!== undefined){
            for(var i=0;i<usados.length;i++){
                if(usados [i] == num){
                    existe = true;
                    break;
                }
            }
            if(!existe){
                //console.log("no existe");
                //console.dir(this.usados);
                usados[usados.length] = num;
            }else{
                //console.log("si existe");
                //console.dir(usados);
                numeroAleatorio(min, max);
            }
        }

        return num;
    }

// funcion que quita los slashes de las cadenas 
function dropSlashes(rawString) {
    
    var raw = "";
    for(var i = 0; i < rawString.length; i++)
    {
        if(rawString[i] != '/' && rawString[i] != '%' && rawString[i] != '-') 
        {
            raw += rawString[i] ;
        }    
    }  

    return raw;
}
    /*
async function aleatorio(min, max) {

    //if (usados.length !=(max-min)) {
    while (repe != false) {
    var num = Math.floor(Math.random()*(max-min+1))+min;
        var repe = repetido(num);
}
    usados.push(num);
    return num;
//}else {
//return null;
//}
    }



    async function repetido(num){
    var repe = false;
    for (i=0; i<usados.length; i++) {
    if (num == usados[i]) {
    repe = true;
    }
    }
    return repe;
    }

*/


