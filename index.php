<?php include 'header.php' ?> 
<div id="cont_main">
    <main class="home d-flex w-100 h-100 justify-content-center align-items-center" id="main_search" style="">

        <form class="form-inline d-flex flex-column justify-content-center">
            <h1 class="mb-5"><span class="fleft">Tengo</span><input type="number" id="minutes" name="minutes" class="form-control fleft mx-3" min="1" max="125" required> <span class="fleft">minutos para ver algo en Netflix.</span></h1>
            <a class="btn btn-flix" id="findByTimeBtn">Buscar</a>
        </form>
    </main>
</div>
<div class="w-100 resultados" id="main_resultados" style="display:none">

    <div class="container">

        <div class="row">

            <div class="col-12 text-left">

                <h1>En <span id="tiempo_buscado"></span> minutos podría ver: </h1>

            </div>

            <div class="col-12 text-left mb-5 pb-3">
                <!--<h4 id="cant_resultados"></h4>-->
                <?php /* <button id="alphBnt">Ordenar alfabéticamente</button> */?>
                <button id="numBnt" class="ml-0">Ordenar por duración</button>


            </div>

        </div>

    </div>


    <div class="container-fluid">

        <div class="row">

            <div class="col-12 text-center d-flex flex-wrap justify-content-center contenido" id="contenido">



            </div>
            <div class="col-12 pt-0 pb-5 text-center">
                <p class="mb-3">Disclaimer: Ciertos contenidos pueden no estar disponibles en tu región - Timeflix no está asociado de ninguna manera con Netflix. </p>

                <a id="mostrar_mas" class="btn btn-flix ml-3 mb-3">Mostrar mas resultados</a>
                <a href="index.php"  class="btn btn-flix ml-3 mb-3">Buscar de nuevo</a>

                
            </div>





        </div>

    </div>

</div>

<?php include 'footer.php' ?> 