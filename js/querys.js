async function get_total_pages(){
 
    var total_pages = 0; 

    $.ajax({
        type: "POST",
        async:false,
        url: "./php/get_total_pages.php",         
        success: function (data) { 
            total_pages = data; 
        }
        }).fail(function (jqXHR, textStatus, errorThrown) {

        if (jqXHR.status === 0) {

            alert('Not connect: Verify Network.');

        } else if (jqXHR.status == 404) {

            alert('Requested page not found [404]');

        } else if (jqXHR.status == 500) {

            alert('Internal Server Error [500].');

        } else if (textStatus === 'parsererror') {

            alert('Requested JSON parse failed.');

        } else if (textStatus === 'timeout') {

            alert('Time out error.');

        } else if (textStatus === 'abort') {

            alert('Ajax request aborted.');

        } else {

            alert('Uncaught Error: ' + jqXHR.responseText);

        } 
    });

    return total_pages;
}
 
async function get_series(pag_aleatoria, duracion) {

    var json = "";
    var jsonquery = { pag_aleatoria: pag_aleatoria , duracion: duracion};

    $.ajax({
        type: "POST",
        async: false,
        url: "./php/get_series.php",
        data: { json: JSON.stringify(jsonquery)},
        success: function (data) {              
            json = data;
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {

        if (jqXHR.status === 0) {

            alert('Not connect: Verify Network.');

        } else if (jqXHR.status == 404) {

            alert('Requested page not found [404]');

        } else if (jqXHR.status == 500) {

            alert('Internal Server Error [500].');

        } else if (textStatus === 'parsererror') {

            alert('Requested JSON parse failed.');

        } else if (textStatus === 'timeout') {

            alert('Time out error.');

        } else if (textStatus === 'abort') {

            alert('Ajax request aborted.');

        } else {

            alert('Uncaught Error: ' + jqXHR.responseText);

        }
    });
         
    return jQuery.parseJSON(json);  
} 

async function getDetails_IDs(details) {

    var json_details = ""; 

    $.ajax({
        type: "POST",
        async: false,
        url: "./php/get_detail_id.php",
        data: { json: JSON.stringify(details) },
        success: function (data) {  
            json_details = data;  
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {

        if (jqXHR.status === 0) {

            alert('Not connect: Verify Network.');

        } else if (jqXHR.status == 404) {

            alert('Requested page not found [404]');

        } else if (jqXHR.status == 500) {

            alert('Internal Server Error [500].');

        } else if (textStatus === 'parsererror') {

            alert('Requested JSON parse failed.');

        } else if (textStatus === 'timeout') {

            alert('Time out error.');

        } else if (textStatus === 'abort') {

            alert('Ajax request aborted.');

        } else {

            alert('Uncaught Error: ' + jqXHR.responseText);

        }
    });
 
    return jQuery.parseJSON(json_details);
     
}
