
    $(document).ready(function(){
        var divs;
        var pagina = 1;
        var n_aleatorio;
        var usados = new Array();
        var cantidad_paginas;

        $('#alphBnt').on('click', function () {
            var alphabeticallyOrderedDivs = $divs.sort(function (a, b) {
                return $(a).find("h1").text() > $(b).find("h1").text();
            });
            $("#contenido").html(alphabeticallyOrderedDivs);
        });

        $('#numBnt').on('click', function () {
            var numericallyOrderedDivs = $divs.sort(function (a, b) {
                return $(a).find("h2").text() - $(b).find("h2").text();
            });
            $("#contenido").html(numericallyOrderedDivs);
        });

        $("#findByTimeBtn").on('click',function(){
            $("#loa").show();
            setTimeout(function(){ 
                $("#tiempo_buscado").html($("#minutes").val()); 
                $("#main_search").css('filter','blur(6px)');
                findByTime($("#minutes").val(), usados);
            }, 1000);

        });
        $("#mostrar_mas").on('click',function(){

            $("#loa").show();
            setTimeout(function(){
                $("#tiempo_buscado").html($("#minutes").val());
                $("#main_search").css('filter','blur(6px)');
                mostrarMas($("#minutes").val(), usados);
            }, 1000);

        });
    });

    async function getDetails(length_json, results)
    {
        if (length_json > 0) {
            var json = {};
            var peliculas = []
            json.peliculas = peliculas;

            results.items.forEach(element => {
                var details = {
                    "id": element.id,
                    "tipo": element.object_type
                }
                json.peliculas.push(details);
            });
            details = await getDetails_IDs(json);
            return details;
        }
    }

    async function findByTime(time, usados){ //Funcion para encontrar series por tiempo
        var req = new XMLHttpRequest();
        cantidad_peliculas = 0;
        try {
            //obtener el valor de paginas para consulta, luego consultamos nuevamente para hacer búsquedas aleatorias.
            cantidad_paginas = await get_total_pages();

            n_aleatorio = await numeroAleatorio(1, cantidad_paginas, usados);
            await usados.push(n_aleatorio);

            var results = await get_series(n_aleatorio, time); 
            var length_json = Object.keys(results.items).length;

            // obtener los detalles de las peliculas/series
            var details;
            if (length_json > 0)
            {
                details = await getDetails(length_json, results);
            }

            for (var i = 0; i < length_json; i++){

                var id = results.items[i].id;
                var tipo = results.items[i].object_type;
                var nb_slug = results.items[i].full_path.split("/");
                var slug = nb_slug[3];
                var imagen;
                var imagen_id;
                var ruta_imagen;

                if (results.items[i].poster !== undefined) {

                    ruta_imagen = results.items[i].poster.split("/");
                    imagen_id = ruta_imagen[1] + "/" + ruta_imagen[2] + "/";
                    imagen = "/" + imagen_id + "s166/" + slug;
                } else {
                    imagen = null;
                }
                var titulo = results.items[i].title;
                var duracion = results.items[i].runtime;

                if (duracion !== null) {
                    if (imagen !== null) {
                        cantidad_peliculas++;
                        await this.addMovie(id, i, imagen, titulo, duracion, tipo, details[i]);
                    } else {
                        console.log("no posee imagen: " + i);
                    }

                } else {
                    if (tipo === "show") {
                        if (imagen !== null) {
                            cantidad_peliculas++;
                            await this.addMovie(id, i, imagen, titulo, "", tipo, details[i]);
                        }
                        console.log("no posee duracion: serie");
                    }
                }
            }
            
            $divs = $("div.item");
            $("#cont_main").css('display', 'none');
            $("#main_resultados").show();
            $("#loa").hide();
            $("#main_search").css('filter', 'none');
            $("#cant_resultados").html(cantidad_peliculas + " resultados en esta búsqueda.");
            //$("#lbl_pagina").html("Página #"+(parseInt($("#pagina").val())+1));
            $("#pagina").val(parseInt($("#pagina").val()) + 1);

        } catch(err) {
            alert(err);

            $("#main_search").css('filter','blur(0px)');
            $("#loa").hide();
        }
    }

    async function mostrarMas(time, usados){
        cantidad_peliculas = 0;
        n_aleatorio = await numeroAleatorio(1, cantidad_paginas, usados);

        await usados.push(n_aleatorio);
        try {
            var results = await get_series(n_aleatorio, time);
            var length_json = Object.keys(results.items).length;
            
            // obtener los detalles de las peliculas/series
            var details;
            if (length_json > 0) {
                details = await getDetails(length_json, results);
            }

            for (var i = 0; i < length_json; i++){

                var id = results.items[i].id;
                var tipo = results.items[i].object_type;

                var nb_slug = results.items[i].full_path.split("/");
                var slug = nb_slug[3];
                if(results.items[i].poster !== undefined){
                    var ruta_imagen = results.items[i].poster.split("/");
                    var imagen_id = ruta_imagen[1]+"/"+ruta_imagen[2]+"/";
                    var imagen = "/"+imagen_id+"s166/"+slug;
                }else{
                    imagen = null;
                }

                var titulo = results.items[i].title;
                var duracion = results.items[i].runtime;

                if((duracion !== null)){
                    if(imagen !== null){
                        cantidad_peliculas++;

                        await this.addMovie(id, i, imagen, titulo, duracion, tipo, details[i]);
                    } 

                }else{
                    if(tipo === "show"){
                        if(imagen !== null){
                            cantidad_peliculas++;
                            //console.log("hola");
                            await this.addMovie(id,i,imagen,titulo,"",tipo, details[i]);
                        } 
                    }
                }
            }

            $divs = $("div.item");
            $("#cont_main").css('display', 'none');
            $("#main_resultados").show();
            $("#loa").hide();
            $("#main_search").css('filter', 'none');
            $("#cant_resultados").html(cantidad_peliculas + " resultados.");
            $("#pagina").val(parseInt($("#pagina").val()) + 1);

        } catch(err) {
            alert(err);
            console.trace();
        }
    }

    async function addMovie(id,posicion,imagen,titulo,duracion,tipo, details){
        let result = JSON.parse(details.data)
        var urlcod = result.url_code;
        duracion = result.runtime;

        //variable que verifica si el item es una pelicula o una serie
        var esSerie = "Movie";
        if(tipo === "show"){
            esSerie = "Show";
        }

        $('.contenido').append(`<div class="item">
            <h1 hidden>${titulo}</h1>
            <h2 hidden>${duracion}</h2>
            <div class="img position-relative">

            <span class="position-absolute mins" style='background-color:rgba(0,0,0,0.7);' >${duracion} min</span>
            <!--<span class="position-absolute" style='background-color:rgba(0,0,0,0.7); padding-bottom:5px; padding-top:5px; padding-right:5px; padding-left:3px;' >${esSerie}</span>-->
            <img src="https://images.justwatch.com${imagen}" alt="" width="166" height:"236">
            <a href="${urlcod}" target="_blank" class="btn-mob">&nbsp;</a>
            <div class="position-absolute info">
            <h5>${titulo}</h5>

            <a href="${urlcod}" target="_blank" class="btn btn-flix" >Ver en Netflix</a> 

                </div> 
                </div> 
                </div>`);
    } 

    //funcion para definir el número aleatorio y validar nueva pagina que no este repetida en el array
    async function numeroAleatorio(min, max, usados) {
        var num = Math.round(Math.random() * (max - min) + min);
        var existe = false; 
        if (usados.length !== undefined) {
            for (var i = 0; i < usados.length; i++) {
                if (usados[i] == num) {
                    existe = true;
                    break;
                }
            }
            if (existe) {
                numeroAleatorio(min, max);
            } else {
                usados[usados.length] = num;
            }
        }

        return num;
    }

    


